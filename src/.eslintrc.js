module.exports = {
    env: {
        browser: true,
        es2021: true,
    },
    parser: "@typescript-eslint/parser",
    extends: [
        "eslint:recommended",
        "react-app",
        "prettier",
        "plugin:@typescript-eslint/recommended",
        "plugin:storybook/recommended",
    ],
    overrides: [],
    parserOptions: {
        ecmaVersion: "latest",
        sourceType: "module",
    },
    plugins: ["react", "@typescript-eslint"],
    rules: {},
};
