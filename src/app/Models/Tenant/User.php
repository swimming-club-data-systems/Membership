<?php

namespace App\Models\Tenant;

use App\Business\Helpers\Address;
use App\Exceptions\Accounting\JournalAlreadyExists;
use App\Mail\VerifyEmailChange;
use App\Models\Accounting\Journal;
use App\Models\Central\Tenant;
use App\Models\Tenant\Auth\UserCredential;
use App\Models\Tenant\Auth\V1Login;
use App\Traits\Accounting\AccountingJournal;
use App\Traits\BelongsToTenant;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;
use Laravel\Passport\HasApiTokens;
use Laravel\Scout\Searchable;
use Stripe\Exception\InvalidRequestException;

use function Illuminate\Events\queueable;

/**
 * @property int $id
 * @property int $UserID
 * @property string $Forename
 * @property string $Surname
 * @property string $Password
 * @property string $EmailAddress
 * @property bool $EmailComms
 * @property string $Mobile
 * @property bool $MobileComms
 * @property bool $Active
 * @property string $name
 * @property string $email
 * @property StripeCustomer $stripeCustomer
 * @property Journal|null $journal
 * @property Carbon $Edit
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $gravatar_url
 * @property Collection $competitionGuestEntryHeaders
 */
class User extends Authenticatable implements MustVerifyEmail
{
    use AccountingJournal, BelongsToTenant, HasApiTokens, HasFactory, Notifiable, Searchable;

    protected bool $configOptionsCached = false;

    protected array $configOptions = [];

    protected bool $permissionsCached = false;

    protected array $permissionsCache = [];

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'Forename',
        'Surname',
        'EmailAddress',
        'Password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'Password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'edit' => 'datetime',
    ];

    protected $primaryKey = 'UserID';

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['gravatar_url'];

    protected static function booted()
    {
        static::addGlobalScope('active', function (Builder $builder) {
            $builder->where('Active', true);
        });

        static::updated(queueable(function (self $user) {
            if ($user->stripeCustomer) {
                /** @var Tenant $tenant */
                $tenant = tenant();

                $address = $user->getAddress();

                try {
                    // Create a new Stripe customer
                    \Stripe\Customer::update(
                        $user->stripeCustomer->CustomerID,
                        [
                            'name' => $user->name,
                            'description' => 'Generated by SCDS Membership',
                            'email' => $user->email,
                            'phone' => $user->Mobile,
                            'preferred_locales' => ['en-GB', 'en'],
                            'address' => [
                                'city' => $address->city,
                                'country' => $address->country_code,
                                'line1' => $address->address_line_1,
                                'line2' => $address->address_line_2,
                                'postal_code' => $address->post_code,
                                // 'state' => $address->county, // No mapping for UK
                            ],
                        ],
                        [
                            'stripe_account' => $tenant->stripeAccount(),
                        ]
                    );
                } catch (InvalidRequestException $e) {
                    // Ignore
                    throw $e;
                }
            }
        }));
    }

    /**
     * get an Address object for the user
     */
    public function getAddress(): Address
    {
        return Address::create($this->getOption('MAIN_ADDRESS'));
    }

    public function getOption($key)
    {
        if (! $this->configOptionsCached) {
            foreach ($this->userOptions()->get() as $option) {
                $this->configOptions[$option->Option] = $option->Value;
            }
            $this->configOptionsCached = true;
        }

        if (isset($this->configOptions[$key])) {
            return $this->configOptions[$key];
        }

        return null;
    }

    /**
     * Get the options for the user.
     */
    public function userOptions(): HasMany
    {
        return $this->hasMany(UserOption::class, 'User');
    }

    /**
     * Send an email to the new address to validate and change
     */
    public function verifyNewEmail(string $email): void
    {
        // User has changed their email
        // The email is not already in use for this tenant
        // Send a signed link to the new email to confirm
        $url = URL::temporarySignedRoute(
            'verification.verify_change',
            now()->addDay(),
            ['user' => Auth::id(), 'email' => Str::lower($email)]
        );

        $recipient = new \stdClass();
        $recipient->email = $email;
        $recipient->name = $this->name;

        Mail::to($recipient)->send(new VerifyEmailChange($this, $url, $email));
    }

    /**
     * Get the user id via expected attribute.
     */
    protected function id(): Attribute
    {
        return Attribute::make(
            get: fn ($value, $attributes) => $attributes['UserID'],
        );
    }

    /**
     * Relationships
     */
    public function setOption($key, $value)
    {
        // Make sure values are cached
        $this->getOption($key);

        // Create or update
        $option = $this->userOptions()->where('Option', $key)->firstOrNew();
        $option->Option = $key;
        $option->Value = $value;
        $option->save();

        // Update cache
        $this->configOptions[$key] = $value;
    }

    /**
     * Get the V1Logins for the user.
     */
    public function v1Logins(): HasMany
    {
        return $this->hasMany(V1Login::class, 'user_id');
    }

    /**
     * Get the WebAuthn User Credentials for the user.
     */
    public function userCredentials(): HasMany
    {
        return $this->hasMany(UserCredential::class, 'user_id');
    }

    /**
     * Get the user's notify category options
     */
    public function notifyCategories(): BelongsToMany
    {
        return $this->belongsToMany(NotifyCategory::class, 'notifyOptions', 'UserID', 'EmailType', 'ID')
            ->as('subscription')
            ->withTimestamps()
            ->withPivot([
                'Subscribed',
            ]);
    }

    /**
     * Get the additional emails for the user.
     */
    public function notifyAdditionalEmails(): HasMany
    {
        return $this->hasMany(NotifyAdditionalEmail::class, 'UserID');
    }

    public function members(): HasMany
    {
        return $this->hasMany(Member::class, 'UserID');
    }

    public function payments(): HasMany
    {
        return $this->hasMany(Payment::class, 'user_UserID');
    }

    public function balanceTopUps(): HasMany
    {
        return $this->hasMany(BalanceTopUp::class, 'user_UserID');
    }

    public function representedSquads(): BelongsToMany
    {
        return $this->belongsToMany(Squad::class, 'squadReps', 'User', 'Squad')
            ->withTimestamps()
            ->withPivot([
                'ContactDescription',
            ]);
    }

    public function galas(): BelongsToMany
    {
        return $this->belongsToMany(Gala::class, 'teamManagers', 'User', 'Gala')
            ->withTimestamps();
    }

    public function manualPaymentEntries(): BelongsToMany
    {
        return $this->belongsToMany(ManualPaymentEntry::class);
    }

    public function hasPermission(string|array $name)
    {
        // Fetch cache
        if (! $this->permissionsCached) {
            foreach ($this->permissions()->get() as $permission) {
                $this->permissionsCache[] = $permission->Permission;
            }
            $this->permissionsCached = true;
        }

        if (gettype($name) == 'string') {
            return in_array($name, $this->permissionsCache);
        } elseif (gettype($name) == 'array') {
            foreach ($name as $item) {
                if (in_array($item, $this->permissionsCache)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Get the user's assigned permissions
     */
    public function permissions(): HasMany
    {
        return $this->hasMany(Permission::class, 'User');
    }

    public function onboardingSessions(): HasMany
    {
        return $this->hasMany(OnboardingSession::class, 'user', 'UserID');
    }

    public function statements(): HasMany
    {
        return $this->hasMany(CustomerStatement::class);
    }

    public function competitionGuestEntryHeaders(): HasMany
    {
        return $this->hasMany(CompetitionGuestEntryHeader::class);
    }

    /**
     * Auth stuff
     */
    public function getAuthIdentifierName(): string
    {
        return 'UserID';
    }

    public function getAuthIdentifier(): int
    {
        return $this->UserID;
    }

    public function getAuthPassword(): string
    {
        return $this->Password;
    }

    public function getEmailForPasswordReset(): string
    {
        return $this->EmailAddress;
    }

    public function getEmailAttribute()
    {
        return $this->attributes['EmailAddress'];
    }

    /**
     * Get the user's profile image url.
     */
    public function gravatarUrl(): Attribute
    {
        return new Attribute(
            get: fn () => 'https://www.gravatar.com/avatar/'.md5(mb_strtolower(trim($this->EmailAddress))).'?d=mp',
        );
    }

    public function getEmailForVerification(): string
    {
        return $this->EmailAddress;
    }

    /**
     * Determine if the model should be searchable.
     *
     * @return bool
     */
    public function shouldBeSearchable()
    {
        return $this->Active;
    }

    public function toSearchableArray(): array
    {
        $array = $this->toArray();

        $fields = [
            'UserID',
            'Forename',
            'Surname',
            'EmailAddress',
            'Mobile',
            'Tenant',
        ];

        return array_intersect_key($array, array_flip($fields));
    }

    /**
     * Get Sms messages sent to this user
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function sms()
    {
        return $this->morphToMany(Sms::class, 'smsable');
    }

    public function extraFees(): BelongsToMany
    {
        return $this->belongsToMany(ExtraFee::class, 'extrasRelations', 'UserID', 'ExtraID')
            ->withTimestamps();
    }

    public function stripeCustomerId(): string
    {
        if ($this->stripeCustomer) {
            return $this->stripeCustomer->CustomerID;
        } else {
            /** @var Tenant $tenant */
            $tenant = tenant();

            $address = $this->getAddress();

            // Create a new Stripe customer
            $customer = \Stripe\Customer::create([
                'name' => $this->name,
                'description' => 'Generated by SCDS Membership',
                'email' => $this->email,
                'phone' => $this->Mobile,
                'preferred_locales' => ['en-GB', 'en'],
                'address' => [
                    'city' => $address->city,
                    'country' => $address->country_code,
                    'line1' => $address->address_line_1,
                    'line2' => $address->address_line_2,
                    'postal_code' => $address->post_code,
                    // 'state' => $address->county, // No mapping for UK
                ],
            ], [
                'stripe_account' => $tenant->stripeAccount(),
            ]);

            $stripeCustomer = new StripeCustomer();
            $stripeCustomer->CustomerID = $customer->id;
            $this->stripeCustomer = $stripeCustomer;

            return $stripeCustomer->CustomerID;
        }
    }

    public function getJournal(): MorphOne
    {
        if (! $this->journal()->exists()) {
            try {
                /** @var LedgerAccount $ledger */
                $ledger = LedgerAccount::where('name', 'Customer Income')->where('is_system', true)->first();

                if (! $ledger) {
                    // Can not proceed
                    // throw an error
                    throw new ModelNotFoundException('The System Customer Income Ledger could not be found');
                }

                $this->initJournal('GBP', $ledger->ledger->id);

                // Refresh as the journal is not always consistently returned straight away
                $this->refresh();
            } catch (JournalAlreadyExists $e) {
                // Ignore, we already checked existence
            }
        }

        return $this->journal();
    }

    public function preferredDirectDebit()
    {
        return $this
            ->paymentMethods()
            ->where('type', '=', 'bacs_debit')
            ->where('default', '=', true)
            ->orderBy('created_at', 'asc')
            ->first();
    }

    public function paymentMethods()
    {
        return $this->hasMany(PaymentMethod::class);
    }

    /**
     * Get the user full name via expected attribute.
     */
    protected function name(): Attribute
    {
        return Attribute::make(
            get: fn ($value, $attributes) => $attributes['Forename'].' '.$attributes['Surname'],
        );
    }

    /**
     * Get the user's password via expected attribute.
     */
    protected function password(): Attribute
    {
        return Attribute::make(
            get: fn ($value, $attributes) => $attributes['Password'],
        );
    }

    protected function email(): Attribute
    {
        return Attribute::make(
            get: fn ($value, $attributes) => $attributes['EmailAddress'],
            set: fn ($value) => [
                'EmailAddress' => $value,
            ],
        );
    }

    /**
     * Get the stripe customer for this user
     */
    protected function stripeCustomer()
    {
        return $this->hasOne(StripeCustomer::class, 'User');
    }
}
