<?php

namespace App\Enums;

enum DistanceUnits: string
{
    case METRES = 'metres';
    case YARDS = 'yards';
    case FEET = 'feet';
}
