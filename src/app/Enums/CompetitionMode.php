<?php

namespace App\Enums;

enum CompetitionMode: string
{
    case BASIC = 'basic';
    case FULL = 'full';
}
