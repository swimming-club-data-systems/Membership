const defaultTheme = require("tailwindcss/defaultTheme");

/** @type {import("tailwindcss").Config} */
module.exports = {
    content: [
        "./vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php",
        "./storage/framework/views/*.php",
        "./resources/views/**/*.blade.php",
        "./resources/js/**/*.jsx",
        "./resources/js/**/*.tsx",
        "./stories/**/*.tsx",
        "./stories/**/*.jsx",
        "./stories/**/*.ts",
        "./stories/**/*.js",
        "./node_modules/react-tailwindcss-datepicker/dist/index.esm.js",
    ],

    theme: {
        extend: {
            fontFamily: {
                sans: [
                    '"Inter var", ui-sans-serif, system-ui, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji"',
                    { fontFeatureSettings: '"cv11", "ss01"' },
                ],
            },
            transitionProperty: {
                height: "height",
            },
        },
    },

    plugins: [
        require("@tailwindcss/forms"),
        require("@tailwindcss/typography"),
        require("@tailwindcss/aspect-ratio"),
        require("@tailwindcss/container-queries"),
    ],
};
