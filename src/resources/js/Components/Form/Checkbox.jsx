import React from "react";
import RadioCheck from "./RadioCheck";

const Checkbox = (props) => {
  return <RadioCheck {...props} type="checkbox" />;
};

export default Checkbox;
