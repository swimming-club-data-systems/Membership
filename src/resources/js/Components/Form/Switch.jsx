import React from "react";
import RadioCheck from "./RadioCheck";

const Switch = (props) => {
  return <RadioCheck {...props} type="switch" />;
};

export default Switch;
