import React from "react";

const InternalContainer = ({ children }) => {
  return <div className="mx-auto px-4 sm:px-6">{children}</div>;
};

export default InternalContainer;
