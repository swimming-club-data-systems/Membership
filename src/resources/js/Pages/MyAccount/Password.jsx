import React, { useEffect, useState } from "react";
import MainLayout from "@/Layouts/MainLayout";
import { Head } from "@inertiajs/react";
import Layout from "./Layout";
import Form, {
    RenderServerErrors,
    SubmissionButtons,
} from "@/Components/Form/Form";
import * as yup from "yup";
import Card from "@/Components/Card";
import FlashAlert from "@/Components/FlashAlert";
import TextInput from "@/Components/Form/TextInput";
import BasicList from "@/Components/BasicList";
import Button from "@/Components/Button";
import Modal from "@/Components/Modal";
import { router } from "@inertiajs/react";
import useRegistration from "@/Pages/Auth/Helpers/useRegistration";
import Alert from "@/Components/Alert";
import { ShieldCheckIcon } from "@heroicons/react/24/outline";
import A from "@/Components/A";
import axios from "@/Utils/axios";
import {
    startRegistration,
    browserSupportsWebAuthn,
} from "@simplewebauthn/browser";

const Password = (props) => {
    const [deleteModalData, setDeleteModalData] = useState(null);
    const [showDeleteModal, setShowDeleteModal] = useState(false);
    const [showTotpModal, setShowTotpModal] = useState(false);
    const [totpModalData, setTotpModalData] = useState(null);
    const [error, setError] = useState(null);
    const [showDisableTotpModal, setShowDisableTotpModal] = useState(false);
    const [canUsePlatformAuthenticator, setCanUsePlatformAuthenticator] =
        useState(false);

    useEffect(() => {
        (async () => {
            if (await browserSupportsWebAuthn()) {
                setCanUsePlatformAuthenticator(true);
            }
        })();
    }, []);

    const getCookie = (cName) => {
        const name = cName + "=";
        const cDecoded = decodeURIComponent(document.cookie); //to be careful
        const cArr = cDecoded.split("; ");
        let res;
        cArr.forEach((val) => {
            if (val.indexOf(name) === 0) res = val.substring(name.length);
        });
        return res;
    };

    const register = useRegistration(
        {
            actionUrl: route("my_account.webauthn_verify"),
            actionHeader: {
                "X-XSRF-TOKEN": getCookie("XSRF-TOKEN"),
            },
            optionsUrl: route("my_account.webauthn_challenge"),
        },
        {
            "X-XSRF-TOKEN": getCookie("XSRF-TOKEN"),
        }
    );

    const handleRegister = async (ev, formikBag) => {
        let asseResp;

        const request = await axios.post(
            route("my_account.webauthn_challenge"),
            {
                passkey_name: ev.name,
            }
        );

        const options = request.data;
        if (!options.excludeCredentials) {
            options.excludeCredentials = [];
        }

        try {
            // Pass the options to the authenticator and wait for a response
            asseResp = await startRegistration(options);
        } catch (error) {
            // Some basic error handling
            formikBag.setSubmitting(false);
            setError(error.message);
            return;
        }

        // POST the response to the endpoint that calls
        // @simplewebauthn/server -> verifyAuthenticationResponse()
        const verificationResponse = await axios.post(
            route("my_account.webauthn_verify"),
            asseResp
        );

        if (verificationResponse.data.success) {
            formikBag.resetForm();
            setError(null);
            router.reload({
                only: ["passkeys", "flash"],
                preserveScroll: true,
            });
        } else {
            setError("Invalid registration");
        }
        formikBag.setSubmitting(false);
    };

    const handleTotpSave = async (values, formikBag) => {
        try {
            router.post(route("my_account.save_totp"), values, {
                onSuccess: (arg) => {
                    if (arg.props.has_totp) {
                        setShowTotpModal(false);
                    }
                    formikBag.resetForm();
                },
                preserveScroll: true,
            });
        } catch (error) {
            // setError(error.message);
        }
    };

    const deletePasskey = async () => {
        router.delete(route("my_account.webauthn_delete", deleteModalData.id), {
            preserveScroll: true,
            onFinish: (page) => {
                setShowDeleteModal(false);
            },
        });
    };

    const loadAndShowTotp = async () => {
        let result = await axios.get(route("my_account.create_totp"));
        setTotpModalData(result.data);
        setShowTotpModal(true);
    };

    const closeTotp = () => {
        setShowTotpModal(false);
        router.get(
            route("my_account.security"),
            {},
            {
                preserveScroll: true,
                preserveState: true,
                only: ["flash"],
            }
        );
    };

    const disableTotp = () => {
        router.delete(route("my_account.delete_totp"), {
            preserveScroll: true,
            preserveState: true,
            only: ["flash", "has_totp"],
            onSuccess: () => {
                setShowDisableTotpModal(false);
            },
        });
    };

    const hasTotp = props.has_totp;
    const totpButtons = (setup) => {
        if (setup) {
            return (
                <Button
                    type="button"
                    variant="danger"
                    onClick={() => setShowDisableTotpModal(true)}
                >
                    Disable authenticator app
                </Button>
            );
        }
        return (
            <Button type="button" variant="primary" onClick={loadAndShowTotp}>
                Set up authenticator app
            </Button>
        );
    };

    // route('my_account.create_totp');

    return (
        <>
            <Head title="My Account" />

            {/*<Container noMargin className="py-12"></Container>*/}

            <div className="space-y-6 sm:px-6 lg:px-0 lg:col-span-9">
                {/* <form action="#" method="POST"> */}
                <Form
                    initialValues={{
                        name: "",
                    }}
                    validationSchema={yup.object().shape({
                        name: yup
                            .string()
                            .required("A name is required for your passkey"),
                    })}
                    // action={route("my_account.additional_email")}
                    submitTitle="Add passkey"
                    hideClear
                    // hideDefaultButtons
                    hideErrors
                    removeDefaultInputMargin
                    formName="manage_passkeys"
                    onSubmit={handleRegister}
                    hideDefaultButtons
                >
                    <Card
                        footer={
                            canUsePlatformAuthenticator ? (
                                <SubmissionButtons />
                            ) : null
                        }
                    >
                        <div>
                            <h3 className="text-lg leading-6 font-medium text-gray-900">
                                Passkeys
                            </h3>
                            <p className="mt-1 text-sm text-gray-500">
                                Passwordless login for your club account.
                            </p>
                        </div>

                        <FlashAlert className="mb-4" bag="delete_credentials" />

                        {props.passkeys.length > 0 && (
                            <BasicList
                                items={props.passkeys.map((item) => {
                                    return {
                                        id: item.id,
                                        content: (
                                            <>
                                                <div
                                                    className="flex align-middle justify-between text-sm"
                                                    key={item.id}
                                                >
                                                    <div className="">
                                                        <div className="text-gray-900">
                                                            {item.name}
                                                        </div>
                                                        <div className="text-gray-500">
                                                            Created at{" "}
                                                            {new Date(
                                                                item.created_at
                                                            ).toLocaleString()}
                                                        </div>
                                                    </div>
                                                    <div className="">
                                                        <Button
                                                            variant="danger"
                                                            onClick={() => {
                                                                setShowDeleteModal(
                                                                    true
                                                                );
                                                                setDeleteModalData(
                                                                    item
                                                                );
                                                            }}
                                                        >
                                                            Delete
                                                        </Button>
                                                    </div>
                                                </div>
                                            </>
                                        ),
                                    };
                                })}
                            />
                        )}

                        {/*<RenderServerErrors />*/}
                        {/*<FlashAlert className="mb-4" />*/}
                        {error && (
                            <Alert variant="error" title="Error">
                                {error}
                            </Alert>
                        )}

                        {canUsePlatformAuthenticator && (
                            <>
                                <RenderServerErrors />
                                <FlashAlert
                                    className="mb-4"
                                    bag="manage_passkeys"
                                />

                                <div className="grid grid-cols-6 gap-6">
                                    <div className="col-span-6 sm:col-span-3">
                                        <TextInput
                                            name="name"
                                            label="Passkey name"
                                            help="Name your passkey to help you identify the device or keychain it is stored in."
                                        />
                                    </div>
                                </div>
                            </>
                        )}
                    </Card>
                </Form>

                <Modal
                    show={showDeleteModal}
                    onClose={() => setShowDeleteModal(false)}
                    variant="danger"
                    title="Delete passkey"
                    buttons={
                        <>
                            <Button variant="danger" onClick={deletePasskey}>
                                Confirm
                            </Button>
                            <Button
                                variant="secondary"
                                onClick={() => setShowDeleteModal(false)}
                            >
                                Cancel
                            </Button>
                        </>
                    }
                >
                    {deleteModalData && (
                        <p>
                            Are you sure you want to delete{" "}
                            {deleteModalData.name}?
                        </p>
                    )}
                </Modal>

                <Form
                    initialValues={{
                        password: "",
                        password_confirmation: "",
                    }}
                    validationSchema={yup.object().shape({
                        password: yup
                            .string()
                            .required("Please enter a new password"),
                        password_confirmation: yup
                            .string()
                            .required("Please confirm your new password")
                            .oneOf(
                                [yup.ref("password"), null],
                                "Passwords must match"
                            ),
                    })}
                    action={route("my_account.security")}
                    submitTitle="Change password"
                    hideClear
                    hideDefaultButtons
                    hideErrors
                    removeDefaultInputMargin
                    method="put"
                >
                    <Card footer={<SubmissionButtons />}>
                        <div>
                            <h3 className="text-lg leading-6 font-medium text-gray-900">
                                Password
                            </h3>
                            <p className="mt-1 text-sm text-gray-500">
                                Change your account password.
                            </p>
                        </div>

                        <RenderServerErrors />
                        <FlashAlert className="mb-4" />

                        <div className="grid grid-cols-6 gap-6">
                            <div className="col-span-6 sm:col-span-4">
                                <TextInput
                                    name="password"
                                    type="password"
                                    label="New password"
                                    autoComplete="new-password"
                                />
                            </div>

                            <div className="col-span-6 sm:col-span-4">
                                <TextInput
                                    name="password_confirmation"
                                    type="password"
                                    label="Confirm new password"
                                    autoComplete="new-password"
                                />
                            </div>
                        </div>
                    </Card>
                </Form>

                <Card footer={totpButtons(hasTotp)}>
                    <div>
                        <h3 className="text-lg leading-6 font-medium text-gray-900">
                            Two-factor authenticator app
                        </h3>
                        <p className="mt-1 text-sm text-gray-500">
                            An additional layer of security for your account.
                        </p>
                    </div>

                    <Alert
                        variant="warning"
                        title="You may want to consider using a passkey instead of app based 2FA"
                    >
                        We&apos;re rolling out FIDO 2 Passkey support. Passkeys
                        are far more secure than traditional passwords and
                        can&apos;t be phished. They&apos;re also a form of
                        two-factor authentication as they require something you
                        have and something you either are (biometrics) or know
                        (your phone password).
                    </Alert>

                    <FlashAlert bag="totp" className="mb-4" />

                    {hasTotp && (
                        <>
                            <p className="text-sm mb-4">
                                You can disable your two-factor authenticator
                                app at any time. If you do, we&apos;ll go back
                                to sending you authentication codes by email.
                            </p>

                            <p className="text-sm mb-4">
                                If you want to enable your two-factor
                                authenticator app again, you&apos;ll need to
                                complete the set up process again.
                            </p>

                            <p className="text-sm">
                                You can still receive authentication codes by
                                email at any time, for example if you don&apos;t
                                have your authenticator app to hand.
                            </p>
                        </>
                    )}

                    {!hasTotp && (
                        <>
                            <p className="text-sm text-gray-500 mb-4">
                                By default we send en email to your account
                                containing a verification code when you attempt
                                to log in. Setting up an authenticator app
                                achieves the same thing, but your app can
                                generate codes even when you have no connection.
                            </p>

                            <p className="text-sm text-gray-500">
                                This is called a Time-based One-Time Password.
                                It&apos;s an open standard supported by a range
                                of authenticator apps such as Microsoft
                                Authenticator and Google Authenticator. Safari
                                on Mac and iOS also has built in support, as do
                                a variety of password managers.
                            </p>
                        </>
                    )}
                </Card>

                <Modal
                    show={showTotpModal}
                    onClose={closeTotp}
                    Icon={ShieldCheckIcon}
                    title="Set up authenticator app"
                >
                    <Form
                        initialValues={{
                            code: "",
                        }}
                        validationSchema={yup.object().shape({
                            code: yup
                                .string()
                                .length(6, "Authentication codes are 6 digits")
                                .required(
                                    "You must enter an authentication code"
                                )
                                .matches(
                                    /[0-9]{6,6}/,
                                    "Authentication codes are 6 digits"
                                ),
                        })}
                        onSubmit={handleTotpSave}
                        submitTitle="Confirm code"
                        clearTitle="Cancel"
                        hideErrors
                        removeDefaultInputMargin
                        alwaysDirty
                        onClear={closeTotp}
                        alwaysClearable
                    >
                        {totpModalData && (
                            <div className="d-block">
                                <p className="mb-4">
                                    Scan the image below with the two-factor
                                    authentication app on your phone.
                                </p>
                                <p className="mb-4">
                                    If you&apos;re using your phone,{" "}
                                    <A href={totpModalData.url}>
                                        set up automatically
                                    </A>
                                    . If you can’t use a QR code, enter the text
                                    code below the QR code instead.
                                </p>

                                <img
                                    src={totpModalData.image}
                                    className="img-fluid mb-4 mx-auto sm:ml-0"
                                    srcSet={`${totpModalData.image2x} 2x, ${totpModalData.image3x} 3x`}
                                />

                                <p className="mb-4">{totpModalData.key}</p>

                                <FlashAlert bag="totp_modal" className="mb-4" />

                                <TextInput
                                    name="code"
                                    label="Enter the code from the application"
                                    help="After scanning the QR code, the app will display a code that you can enter above."
                                />
                            </div>
                        )}
                    </Form>
                </Modal>

                <Modal
                    show={showDisableTotpModal}
                    onClose={() => setShowDisableTotpModal(false)}
                    variant="danger"
                    title="Disable your two-factor authenticator app"
                    buttons={
                        <>
                            <Button variant="danger" onClick={disableTotp}>
                                Confirm
                            </Button>
                            <Button
                                variant="secondary"
                                onClick={() => setShowDisableTotpModal(false)}
                            >
                                Cancel
                            </Button>
                        </>
                    }
                >
                    <p>
                        Are you sure you want to disable your two-factor
                        authenticator app?
                    </p>
                </Modal>
            </div>
        </>
    );
};

Password.layout = (page) => (
    <MainLayout title="My Account" subtitle="Manage your personal details">
        <Layout children={page} />
    </MainLayout>
);

export default Password;
