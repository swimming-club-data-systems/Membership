---
title: Create a gala
---

# Adding a competition

It's really easy to add a gala to the Membership System.

1. Head to the *Galas* page from the main menu
2. Press *Add a gala*
3. Fill in all the details
4. Submit the form
5. That's it. All done!

Depending on the options selected, the system may prompt you to provide some additional information.

## Editing a gala

You can edit any of the information about a gala at any time.

Head to the *Galas* page from the main menu and select the gala from the list shown. Once on the gala page. Select *Edit* from the top right.

From there, modify any details and save the form.