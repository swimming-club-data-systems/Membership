# Competitions

You can enter competitions online with the SCDS Membership System.

We've started to roll out a new version of our competition entry tools. All clubs will move to this new version in due course. 

Please view the documentation for the version used by your club.

To identify which version your club uses, look at the url in your web browser.

* V1 can be found at `{domain}/v1/galas/`
* V2 can be found at `{domain}/competitions/`