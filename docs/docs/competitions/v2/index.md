# Competitions (V2)

You can enter competitions online with the SCDS Membership System.

The documentation in this section is for the competition entry tools which can be found at `{domain}/competitions/`.