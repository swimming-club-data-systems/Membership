# Tenant administration

You can control a wide range of settings within your tenant application. Some account management features and billing are managed through [SCDS System Administration](https://myswimmingclub.uk/login). This allows you to access your billing account even if your subscription lapses.

Help and support information for tenants as well as information about additional services such as custom domain names provided by SCDS.