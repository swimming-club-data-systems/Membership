---
title: COVID-19 features
---

# Coronavirus (COVID-19) features

:::info

All COVID-19 regulations in England have now been withdrawn. COVID-19 features will remain available for the forseeable future.

:::

## SCDS Coronavirus Response

SCDS responded quickly to the coronavirus pandemic by introducing additional online tools such as [log books](../log-books).

SCDS has continued to implement new features as the pandemic progressed, such as contact tracing, risk awareness forms and health surveys following the Swim England templates.

## Useful websites

* [GOV.UK Coronavirus pages](https://www.gov.uk/coronavirus)
* [NHS Coronavirus pages](https://www.nhs.uk/coronavirus)
* [Swim England](https://www.swimming.org/swimengland/)