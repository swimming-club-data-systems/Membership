# Risk Awareness Forms

:::info

All COVID-19 regulations in England have now been withdrawn. COVID-19 features will remain available for the forseeable future.

:::

## To complete your risk awareness form

* Login using your email address and password (you can reset this if you’ve forgotten it)
* You **may** be sent an authentication code to your email address, please enter the six digit number and click verify
* Select *COVID-19* from the main menu
* Health Screening Survey – click Go
* You will see the name of your swimmer(s) – click new submission
* Read the form, complete the boxes and click ‘submit'
* This document is now completed

## Useful websites

* [GOV.UK Coronavirus pages](https://www.gov.uk/coronavirus)
* [NHS Coronavirus pages](https://www.nhs.uk/coronavirus)
* [Swim England](https://www.swimming.org/swimengland/)