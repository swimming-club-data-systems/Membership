# Manual Payment Entries

Manual payment entries replace the *Invoice Payments* feature in Payments V1 and allow you to add additional credits and
debits to multiple user accounts.