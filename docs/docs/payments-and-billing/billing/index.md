# Billing Cycles

The billing cycle is now customisable on an organisation wide basis. User specific billing cycle customisations are on
our roadmap, with support for this built into the design of Payments and Billing (V2).

## Available Settings

You can manage your billing cycle settings, including the dates on which components of the billing run in the System
Settings section of the Membership System.

You can customise the dates for

- Squad and Extra Fee calculations
- Statement generation
- Charging

The default for all of the above is the first day of each month.