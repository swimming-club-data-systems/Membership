# Ledgers and Journals

Ledgers and Journals form the basis of reporting in Payments and Billing (V2).

Each user has their own linked journal, which contains details about all of their transactions, giving you complete
visibility of your club's finances.

Monthly Direct Debit payments are calculated based on the balance of a user's journal, and user journals are only
credited when payments are confirmed as collected, giving a true reflection of amounts owed.

:::caution Heads up

Ledgers and Journals in SCDS Membership are used to drive reporting and tracking money owed by members. They are not
intended for use as a nominal (general) ledger, and in fact don't provide the required tools for that.

SCDS recommends all clubs make use of professional accounting software, such
as [Sage Accounting](https://www.sage.com/en-gb/sage-business-cloud/accounting/). Speak to an accountant to help you
understand what's best for your organisation.

:::